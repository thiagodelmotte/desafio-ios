//
//  Config.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

// Singleton
+(Config *)shared;

@property (nonatomic, strong) NSDictionary *color;
@property (nonatomic, strong) NSString *api;

@end
