//
//  Config.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "Config.h"

@implementation Config

+(Config *)shared {
    
    static Config *shared = nil;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
    
}

-(id)init {
    
    if (self = [super init]) {
        
        self.color = @{
                       @"navigation": @"27272b"
                       };
        
        self.api = @"https://api.github.com";
        
    }
    
    return self;
    
}

@end
