//
//  Helper.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Helper : NSObject

// Singleton
+(Helper *)shared;

-(UIColor *)UIColorFromHex:(NSString *)hex;
-(NSString *)formatDateTimezone:(NSDate *)date;

@end
