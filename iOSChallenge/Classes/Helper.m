//
//  Helper.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "Helper.h"

@implementation Helper

+(Helper *)shared {
    
    static Helper *shared = nil;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
    
}

-(id)init {
    
    if (self = [super init]) {
        
    }
    
    return self;
    
}

-(UIColor *)UIColorFromHex:(NSString *)hex {

    unsigned int c;
    
    if ([hex characterAtIndex:0] == '#') {
        [[NSScanner scannerWithString:[hex substringFromIndex:1]] scanHexInt:&c];
    } else {
        [[NSScanner scannerWithString:hex] scanHexInt:&c];
    }
    
    return [UIColor colorWithRed:((c & 0xff0000) >> 16)/255.0 green:((c & 0xff00) >> 8)/255.0 blue:(c & 0xff)/255.0 alpha:1.0];
    
}

-(NSString *)formatDateTimezone:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    
    return [formatter stringFromDate:date];
    
}

@end
