//
//  ReposDetailViewController.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReposDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) NSString *repoName;
@property (nonatomic, weak) NSString *repoUserNick;

@end
