//
//  ReposDetailViewController.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "ReposDetailViewController.h"
#import "CustomReposDetailTableViewCell.h"
#import "CustomHeaderTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Helper.h"
#import "Config.h"
#import <AFNetworking.h>
#import "reposDetail.h"

@interface ReposDetailViewController () {
    
    Config *config;
    Helper *helper;
    NSMutableArray *items;
    int totalOpen;
    int totalClose;
    
}

@end

@implementation ReposDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    config = [Config shared];
    helper = [Helper shared];
    
    [self configInterface];
    [self requestPRData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configInterface {
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = self.repoName;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [helper UIColorFromHex:[config.color objectForKey:@"navigation"]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
}


#pragma mark - API -

-(void)requestPRData {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", config.api, [NSString stringWithFormat:@"/repos/%@/%@/pulls", self.repoUserNick, self.repoName]];
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if (error) {
            NSLog(@"Error1: %@", error);
        } else {
            
            if ([httpResponse statusCode] == 200) {
                items = [[NSMutableArray alloc] initWithArray:responseObject];
                [self calcIndex];
                [self.tableView reloadData];
            } else {
                NSLog(@"Error2: %@", error);
            }
            
        }
        
    }];
    
    [dataTask resume];
    
}

-(void)calcIndex {
    
    for (int i=0; i<items.count; i++) {

        reposDetail *repoDetailItem = [MTLJSONAdapter modelOfClass:reposDetail.class fromJSONDictionary:[items objectAtIndex:i] error:nil];
        
        if ([repoDetailItem.state isEqualToString:@"open"]) {
            totalOpen++;
        } else if ([repoDetailItem.state isEqualToString:@"closed"]) {
            totalClose++;
        }
        
    }
    
}


#pragma mark - UITableView Delegates -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = @"reposDetailCell";
    
    CustomReposDetailTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[CustomReposDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    NSDictionary *item = [items objectAtIndex:indexPath.row];
    
    NSError *error;
    reposDetail *repoDetailItem = [MTLJSONAdapter modelOfClass:reposDetail.class fromJSONDictionary:item error:&error];
    
    if (!error) {
        
        cell.repoDetailTitlePR.text = repoDetailItem.title;
        
        cell.repoDetailBodyPR.text = repoDetailItem.body;
        
        [cell.repoDetailavatar sd_setImageWithURL:[NSURL URLWithString:repoDetailItem.urlavatar] placeholderImage:[UIImage imageNamed:@"avatarPlaceholder"]];
        
        cell.repoDetailuserNick.text = repoDetailItem.nick;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *date = [formatter dateFromString:repoDetailItem.date];
        cell.repoDetaildate.text = [helper formatDateTimezone:date];
        
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSError *error;
    reposDetail *repoDetailItem = [MTLJSONAdapter modelOfClass:reposDetail.class fromJSONDictionary:[items objectAtIndex:indexPath.row] error:&error];
    
    if (!error) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:repoDetailItem.urlpr]];
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *cellId = @"headerCell";
    
    CustomHeaderTableViewCell *headerView = [self.tableView dequeueReusableCellWithIdentifier:cellId];
    
    NSString *number = [NSString stringWithFormat:@"%d", totalOpen];
    NSInteger orangeStr = number.length + 8;
    
    headerView.totalOpen.text = [NSString stringWithFormat:@"%d %@ / %d %@", totalOpen, NSLocalizedString(@"opened", nil), totalClose, NSLocalizedString(@"closed", nil)];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:headerView.totalOpen.text];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(0,orangeStr)];
    headerView.totalOpen.attributedText = string;

    return headerView.contentView;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 61;
}

@end
