//
//  ReposListViewController.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReposListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
