//
//  ReposListViewController.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "ReposListViewController.h"
#import "CustomReposTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Helper.h"
#import "Config.h"
#import <AFNetworking.h>
#import "repos.h"
#import "SVPullToRefresh.h"
#import "ReposDetailViewController.h"

@interface ReposListViewController () {
    
    Config *config;
    Helper *helper;
    NSMutableArray *items;
    BOOL appendData;
    BOOL pullToRefresh;
    int page;
    NSString *repoNameClicked;
    NSString *repoUserNickClicked;
    
}

@end

@implementation ReposListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    appendData = NO;
    pullToRefresh = NO;
    page = 1;
    
    config = [Config shared];
    helper = [Helper shared];
    
    [self configInterface];
    [self configInfiniteScroll];
    [self requestData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configInterface {
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = NSLocalizedString(@"repoNavName", nil);
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [helper UIColorFromHex:[config.color objectForKey:@"navigation"]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
}

-(void)configInfiniteScroll {
    
    // setup infinite scrolling
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        
        int64_t delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            page++;
            appendData = YES;
            [self requestData];
            
        });
    }];
    
    // setup pull-to-refresh
    [self.tableView addPullToRefreshWithActionHandler:^{
        
        int64_t delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            page = 1;
            pullToRefresh = YES;
            [self requestData];
            
        });
    }];
    
    [self.tableView triggerPullToRefresh];
    
}


#pragma mark - API -

-(void)requestData {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", config.api, [NSString stringWithFormat:@"/search/repositories?q=language:Java&sort=stars&page=%d", page]];
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if (error) {
            
            NSLog(@"Error1: %@", error);
            
            if (appendData) {
                [self.tableView.infiniteScrollingView stopAnimating];
                if ([httpResponse statusCode] == 422) self.tableView.showsInfiniteScrolling = NO;
                appendData = NO;
            }
            
            if (pullToRefresh) {
                [self.tableView.pullToRefreshView stopAnimating];
                pullToRefresh = NO;
            }
            
        } else {

            if ([httpResponse statusCode] == 200) {
                
                if (appendData) {
                    
                    NSArray *arrNewData = [responseObject objectForKey:@"items"];
                    
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    for (NSInteger i = items.count; i < arrNewData.count + items.count; i++) {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                    
                    [self.tableView beginUpdates];
                    [items addObjectsFromArray:arrNewData];
                    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                    
                    [self.tableView.infiniteScrollingView stopAnimating];
                    
                    appendData = NO;
                    
                } else {
                    
                    items = [[NSMutableArray alloc] initWithArray:[responseObject objectForKey:@"items"]];
                    [self.tableView reloadData];
                    
                    if (pullToRefresh) {
                        [self.tableView.pullToRefreshView stopAnimating];
                        pullToRefresh = NO;
                    }
                    
                }
                
            } else {
                
                NSLog(@"Error2: %@", error);
                
                if (appendData) {
                    [self.tableView.infiniteScrollingView stopAnimating];
                    appendData = NO;
                }
                
                if (pullToRefresh) {
                    [self.tableView.pullToRefreshView stopAnimating];
                    pullToRefresh = NO;
                }
                
            }
            
        }
        
    }];
    
    [dataTask resume];
    
}


#pragma mark - UITableView Delegates -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = @"reposCell";
    
    CustomReposTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[CustomReposTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    NSDictionary *item = [items objectAtIndex:indexPath.row];
    
    NSError *error;
    repos *repoItem = [MTLJSONAdapter modelOfClass:repos.class fromJSONDictionary:item error:&error];
    
    if (!error) {

        cell.repoName.text = repoItem.name;
        
        cell.repoDesc.text = repoItem.desc;
        
        [cell.repoUserAvatar sd_setImageWithURL:[NSURL URLWithString:repoItem.urlavatar] placeholderImage:[UIImage imageNamed:@"avatarPlaceholder"]];
        
        cell.repoUserNick.text = repoItem.nick;
     
        cell.repoForks.text = [NSString stringWithFormat:@"%@", repoItem.forks];
        
        cell.repoFavs.text = [NSString stringWithFormat:@"%@", repoItem.favs];
        
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSError *error;
    repos *repoItem = [MTLJSONAdapter modelOfClass:repos.class fromJSONDictionary:[items objectAtIndex:indexPath.row] error:&error];
    
    if (!error) {
        repoNameClicked = repoItem.name;
        repoUserNickClicked = repoItem.nick;
        [self performSegueWithIdentifier:@"detailVC" sender:self];
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    ReposDetailViewController *detailVC = [segue destinationViewController];
    
    detailVC.repoName = repoNameClicked;
    detailVC.repoUserNick = repoUserNickClicked;
    
}

@end
