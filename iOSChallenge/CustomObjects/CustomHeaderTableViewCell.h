//
//  CustomHeaderTableViewCell.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomHeaderTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *totalOpen;

@end
