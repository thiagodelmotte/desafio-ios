//
//  CustomHeaderTableViewCell.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "CustomHeaderTableViewCell.h"
#import "Helper.h"

@implementation CustomHeaderTableViewCell

- (void)awakeFromNib {
    
    Helper *helper = [Helper shared];
    
    [self.totalOpen setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    self.totalOpen.textColor = [helper UIColorFromHex:@"000000"];
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
