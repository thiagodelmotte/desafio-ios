//
//  CustomReposDetailTableViewCell.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomReposDetailTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *repoDetailTitlePR;
@property (nonatomic, weak) IBOutlet UILabel *repoDetailBodyPR;
@property (nonatomic, weak) IBOutlet UILabel *repoDetailuserNick;
@property (nonatomic, weak) IBOutlet UILabel *repoDetaildate;
@property (nonatomic, weak) IBOutlet UIImageView *repoDetailavatar;

@end
