//
//  CustomReposDetailTableViewCell.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "CustomReposDetailTableViewCell.h"
#import "Helper.h"

@implementation CustomReposDetailTableViewCell

- (void)awakeFromNib {
    
    Helper *helper = [Helper shared];
    
    [self.repoDetailTitlePR setFont:[UIFont fontWithName:@"Arial" size:18]];
    self.repoDetailTitlePR.textColor = [helper UIColorFromHex:@"0c438b"];
    
    [self.repoDetailBodyPR setFont:[UIFont fontWithName:@"Arial" size:15]];
    self.repoDetailBodyPR.textColor = [helper UIColorFromHex:@"181818"];
    
    self.repoDetailavatar.layer.cornerRadius = 30;
    
    [self.repoDetailuserNick setFont:[UIFont fontWithName:@"Arial" size:16]];
    self.repoDetailuserNick.textColor = [helper UIColorFromHex:@"1c659c"];
    self.repoDetailuserNick.adjustsFontSizeToFitWidth = YES;
    self.repoDetailuserNick.minimumScaleFactor = 0.5;
    
    [self.repoDetaildate setFont:[UIFont fontWithName:@"Arial" size:14]];
    self.repoDetaildate.textColor = [helper UIColorFromHex:@"afafaf"];
    self.repoDetaildate.adjustsFontSizeToFitWidth = YES;
    self.repoDetaildate.minimumScaleFactor = 0.5;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
