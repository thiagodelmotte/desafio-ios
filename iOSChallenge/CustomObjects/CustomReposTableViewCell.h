//
//  CustomReposTableViewCell.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomReposTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *repoName;
@property (nonatomic, weak) IBOutlet UILabel *repoDesc;
@property (nonatomic, weak) IBOutlet UILabel *repoForks;
@property (nonatomic, weak) IBOutlet UILabel *repoFavs;
@property (nonatomic, weak) IBOutlet UILabel *repoUserNick;
@property (nonatomic, weak) IBOutlet UIImageView *repoUserAvatar;

@end
