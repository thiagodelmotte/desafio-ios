//
//  CustomReposTableViewCell.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 22/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "CustomReposTableViewCell.h"
#import "Helper.h"

@implementation CustomReposTableViewCell

- (void)awakeFromNib {
    
    Helper *helper = [Helper shared];
    
    [self.repoName setFont:[UIFont fontWithName:@"Arial" size:18]];
    self.repoName.textColor = [helper UIColorFromHex:@"0c438b"];
    
    [self.repoDesc setFont:[UIFont fontWithName:@"Arial" size:15]];
    self.repoDesc.textColor = [helper UIColorFromHex:@"181818"];
    
    self.repoUserAvatar.layer.cornerRadius = 30;
    
    [self.repoUserNick setFont:[UIFont fontWithName:@"Arial" size:16]];
    self.repoUserNick.textColor = [helper UIColorFromHex:@"1c659c"];
    self.repoUserNick.adjustsFontSizeToFitWidth = YES;
    self.repoUserNick.minimumScaleFactor = 0.5;
    
    [self.repoForks setFont:[UIFont fontWithName:@"Arial" size:18]];
    self.repoForks.textColor = [helper UIColorFromHex:@"db7b00"];
    self.repoForks.adjustsFontSizeToFitWidth = YES;
    self.repoForks.minimumScaleFactor = 0.5;
    
    [self.repoFavs setFont:[UIFont fontWithName:@"Arial" size:18]];
    self.repoFavs.textColor = [helper UIColorFromHex:@"db7b00"];
    self.repoFavs.adjustsFontSizeToFitWidth = YES;
    self.repoFavs.minimumScaleFactor = 0.5;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
