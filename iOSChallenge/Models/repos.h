//
//  repos.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <Mantle-HAL/MTLHALResource.h>

@interface repos : MTLHALResource

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSNumber *forks;
@property (nonatomic, strong) NSNumber *favs;
@property (nonatomic, strong) NSString *nick;
@property (nonatomic, strong) NSString *urlavatar;

@end
