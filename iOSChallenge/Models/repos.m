//
//  repos.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "repos.h"

@implementation repos

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"desc": @"description",
             @"forks": @"forks_count",
             @"favs": @"stargazers_count",
             @"nick": @"owner.login",
             @"urlavatar": @"owner.avatar_url"
             };
}

@end
