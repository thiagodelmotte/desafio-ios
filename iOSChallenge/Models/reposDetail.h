//
//  reposDetail.h
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import <Mantle-HAL/Mantle-HAL.h>

@interface reposDetail : MTLHALResource

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSString *urlavatar;
@property (nonatomic, strong) NSString *nick;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *urlpr;
@property (nonatomic, strong) NSString *state;

@end
