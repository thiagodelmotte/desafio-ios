//
//  reposDetail.m
//  iOSChallenge
//
//  Created by Thiago Delmotte on 23/02/16.
//  Copyright © 2016 Thiago Delmotte. All rights reserved.
//

#import "reposDetail.h"

@implementation reposDetail

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title": @"title",
             @"body": @"body",
             @"urlavatar": @"user.avatar_url",
             @"nick": @"user.login",
             @"date": @"created_at",
             @"urlpr": @"html_url",
             @"state": @"state"
             };
}

@end
